#this script gives you required information given the code

#/usr/bin/python
import re

link=raw_input("imdb URL:")

#opening the imdb page
import urllib

from urllib import FancyURLopener

class MyOpener(FancyURLopener):
	version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
myopener = MyOpener()
str=link
print str
page = myopener.open(str)


#writing the source code into a file 
f=open("search_inform.txt","w")
f.write(page.read())
f.close()

#finding the title

pat=re.compile("<title>(.*)\(")
f=open("search_inform.txt","r")

while(1):
	f1=f.readline()
	if(f1):
		result=re.search(pat,f1)
		if result <> None:
			link1=result.group(1)
			print "TITLE :",link1
	else:
		break

#finding the year of release

pat =re.compile("<title>.*\((.*)\)")
f=open("search_inform.txt","r")

while(1):
	f1=f.readline()
	if(f1):
		result=re.search(pat,f1)
		if result <> None:
			link1=result.group(1)
			print "YEAR :",link1
	else:
		break


#finding the Rating of the movie

pat=re.compile('<span class="rating-rating">(.*)<span>')
f=open("search_inform.txt","r")

while(1):
	f1=f.readline()
	if(f1):
		result=re.search(pat,f1)
		if result <> None:
			link1=result.group(1)
			print "RATING :",link1," / 10.0"
	else:
		break


