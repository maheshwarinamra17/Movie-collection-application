# this code gives you the imdb links given the movie name
#/usr/bin/python
import MySQLdb


db=MySQLdb.connect("localhost","root","iiit123","mlibrary")
cursor = db.cursor()

mov=open("movies.txt","r")
fm=mov.readlines()
l=len(fm)
i=1
while(i<=l):
	title=year=rate=direct=cast=genre="NONE"
	fm[i]=fm[i].replace('\n','')
	fm[i]=fm[i].replace('\t','')	
	soft=fm[i]
	
	#opening the google page
	import os
	import urllib
	import re
	from urllib import FancyURLopener
	
	class MyOpener(FancyURLopener):
		version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
	myopener = MyOpener()
	str='http://www.google.com/search?q='+soft+'+movie+imdb'
	
	page = myopener.open(str)
	
	#writing the source code into a file 
	f=open("search.txt","w")
	f.write(page.read())
	f.close()
	
	#finding the pattern
	pat=re.compile("http://www.imdb.com/title/([a-z0-9A-Z]*)/")
	f=open("search.txt","r")
	
	while(1):
		f1=f.readline()
		if(f1):
			result=re.search(pat,f1)
			if result <> None:
				link="http://www.imdb.com/title/"+result.group(1)+"/"
				
		else:
			break
	
	str=link
	
	page = myopener.open(str)
	
	
	#writing the source code into a file 
	f=open("search_inform.txt","w")
	content=page.read()
	f.write(content)
	f.close()
	
	#finding the title
	
	pat=re.compile("<title>(.*)\(")
	f=open("search_inform.txt","r")
	
	while(1):
		f1=f.readline()
		if(f1):
			result=re.search(pat,f1)
			if result <> None:
				title=result.group(1).lstrip()
				
		else:
			break

	
	#finding the year of release
	
	pat =re.compile("<title>.*\((.*)\)")
	f=open("search_inform.txt","r")
	
	while(1):
		f1=f.readline()
		if(f1):
			result=re.search(pat,f1)
			if result <> None:
				year=result.group(1)
				
		else:
			break
	
	
	
	#finding the Rating of the movie
	
	pat=re.compile('<span class="rating-rating">(.*)<span>')
	f=open("search_inform.txt","r")
	
	while(1):
		f1=f.readline()
		if(f1):
			result=re.search(pat,f1)
			if result <> None:
				rate=result.group(1)
				
		else:
			break
	
	
	
	#find some other content
	p=re.sub('<[^>]*>', '', content)
	p=p.replace('\n','')
	p=p.replace('\t','')
	p=p.replace('&nbsp;','')
	
	f=open("no_html.txt","w")
	f.write(p)
	f.close()
	
	#finding the director
	p1=p 			#for safety of not loosing data
	try:
		pat=re.compile("Director:(.*)Stars:")
		m=re.search(pat,p1)
	
		d="Director:"+m.group(1)
	except AttributeError:
		
		pass
	try:
		pat1=re.compile("Director:(.*)Writer")
		m=re.search(pat1,d)
	
		direct=m.group(1).lstrip()
	except AttributeError:
		
		pass

	
	
	#can fail in case of multiple directors
	
	#finding the Stars
	p1=p 			#for safety of not loosing data
		
	try:	
		pat=re.compile("Stars:(.*)Watch Trailer")
		m=re.search(pat,p1)
	
		cast=m.group(1).lstrip()
	except AttributeError:
		
		pass
	
	
	
	#finding the Genre
	try:
		p1=p 			#for safety of not loosing data
		pat=re.compile("Genres:(.*)Motion Picture Rating")
		m=re.search(pat,p1)	
		genre=m.group(1).lstrip()
	
	except AttributeError:
		
		pass

	
	
	#finding the Plot
	p1=p 			#for safety of not loosing data
	try:
		pat=re.compile("Metacritic.\com\)(.*)Director:")
		m=re.search(pat,p1)
	
		plot=m.group(1)
		plot=plot.replace('reviews','')
		m1=re.search("(\d+\s+)(.*)",plot)
	
	
		plot=m1.group(2)
	except AttributeError:
				
		pass
	
	#finding the pattern for image
	pat=re.compile("http://ia.media-imdb.com/images/(.*),214,314_")
	f=open("search_inform.txt","r")
	while(1):
		f1=f.readline()
		if(f1):
			result=re.search(pat,f1)
			if result <> None:
				link="http://ia.media-imdb.com/images/"+result.group(1)+",214,314_.jpg"
				#print link
		else:
			break
	
	
	
	# load a given picture from a web page and save it to a file
	# (you have to be on the internet to do this)
	
	import urllib2
	import webbrowser
	import os
	
	# find yourself a picture on a web page you like
	# (right click on the picture, look under properties and copy the address)
	picture_page = link
	
	#webbrowser.open(picture_page)  # test
	
	# open the web page picture and read it into a variable
	opener1 = urllib2.build_opener()
	page1 = opener1.open(picture_page)
	my_picture = page1.read()
	
	# open file for binary write and save picture
	# picture_page[-4:] extracts extension eg. .gif
	# (most image file extensions have three letters, otherwise modify)
	filename = title +picture_page[-4:] 
	fout = open(filename, "wb")
	fout.write(my_picture)
	fout.close()
	os.system("mv *.jpg poster")
	# saved correctly?
	# test it out ...
	#webbrowser.open(filename)
	print "Done:",i
	i=i+1
	
	
	# Prepare SQL query to INSERT a record into the database.
	sql = "INSERT INTO main(title,year,imdbrating,direct,cast,genre,plot) VALUES ('"+title+"',"+year+","+rate+",'"+direct+"','"+cast+"','"+genre+"','"+plot+"')"
	
	try:
   	# Execute the SQL command
   		cursor.execute(sql)
   	# Commit your changes in the database
  	 	db.commit()
	except:
  	 # Rollback in case there is any error
   		db.rollback()


# disconnect from server
db.close()
	
